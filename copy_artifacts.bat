@echo off

if [%1] == [] GOTO usage
if [%2] == [] GOTO usage

set sourceArtifactsDir=%~1
set targetArtifactsDir=%~2

echo Start copying artifacts from %sourceArtifactsDir% to %targetArtifactsDir%

set srcDir=%sourceArtifactsDir%\Ushu\ttt\22
if EXIST "%srcDir%" (
  echo Copying ttt :: 22 artifacts to ttt :: 14
  xcopy  /i /s "%srcDir%" "%targetArtifactsDir%\Ushu\ttt\14"
)
set srcDir=%sourceArtifactsDir%\Ushu\ttt\20
if EXIST "%srcDir%" (
  echo Copying ttt :: 20 artifacts to ttt :: 13
  xcopy  /i /s "%srcDir%" "%targetArtifactsDir%\Ushu\ttt\13"
)
set srcDir=%sourceArtifactsDir%\Ushu\ttt\13
if EXIST "%srcDir%" (
  echo Copying ttt :: 13 artifacts to ttt :: 12
  xcopy  /i /s "%srcDir%" "%targetArtifactsDir%\Ushu\ttt\12"
)
set srcDir=%sourceArtifactsDir%\Ushu\ttt\11
if EXIST "%srcDir%" (
  echo Copying ttt :: 11 artifacts to ttt :: 11
  xcopy  /i /s "%srcDir%" "%targetArtifactsDir%\Ushu\ttt\11"
)
set srcDir=%sourceArtifactsDir%\Ushu\ttt\10
if EXIST "%srcDir%" (
  echo Copying ttt :: 10 artifacts to ttt :: 10
  xcopy  /i /s "%srcDir%" "%targetArtifactsDir%\Ushu\ttt\10"
)
set srcDir=%sourceArtifactsDir%\Ushu\ttt\9
if EXIST "%srcDir%" (
  echo Copying ttt :: 9 artifacts to ttt :: 9
  xcopy  /i /s "%srcDir%" "%targetArtifactsDir%\Ushu\ttt\9"
)
set srcDir=%sourceArtifactsDir%\Ushu\ttt\8
if EXIST "%srcDir%" (
  echo Copying ttt :: 8 artifacts to ttt :: 8
  xcopy  /i /s "%srcDir%" "%targetArtifactsDir%\Ushu\ttt\8"
)
set srcDir=%sourceArtifactsDir%\Ushu\ttt\7
if EXIST "%srcDir%" (
  echo Copying ttt :: 7 artifacts to ttt :: 7
  xcopy  /i /s "%srcDir%" "%targetArtifactsDir%\Ushu\ttt\7"
)
set srcDir=%sourceArtifactsDir%\Ushu\ttt\5
if EXIST "%srcDir%" (
  echo Copying ttt :: 5 artifacts to ttt :: 5
  xcopy  /i /s "%srcDir%" "%targetArtifactsDir%\Ushu\ttt\5"
)
set srcDir=%sourceArtifactsDir%\Ushu\ttt\4
if EXIST "%srcDir%" (
  echo Copying ttt :: 4 artifacts to ttt :: 4
  xcopy  /i /s "%srcDir%" "%targetArtifactsDir%\Ushu\ttt\4"
)
set srcDir=%sourceArtifactsDir%\Ushu\ttt\3
if EXIST "%srcDir%" (
  echo Copying ttt :: 3 artifacts to ttt :: 3
  xcopy  /i /s "%srcDir%" "%targetArtifactsDir%\Ushu\ttt\3"
)
set srcDir=%sourceArtifactsDir%\Ushu\ttt\2
if EXIST "%srcDir%" (
  echo Copying ttt :: 2 artifacts to ttt :: 2
  xcopy  /i /s "%srcDir%" "%targetArtifactsDir%\Ushu\ttt\2"
)
set srcDir=%sourceArtifactsDir%\Ushu\ttt\1
if EXIST "%srcDir%" (
  echo Copying ttt :: 1 artifacts to ttt :: 1
  xcopy  /i /s "%srcDir%" "%targetArtifactsDir%\Ushu\ttt\1"
)
GOTO end

:usage
echo Usage: %0 sourceArtifactsDir targetArtifactsDir
echo   sourceArtifactsDir     Path to the artifacts directory of the source TeamCity server (usually ^<TeamCity data directory^>/system/artifacts).
echo   targetArtifactsDir     Path to the artifacts directory of the target TeamCity server.

:end
